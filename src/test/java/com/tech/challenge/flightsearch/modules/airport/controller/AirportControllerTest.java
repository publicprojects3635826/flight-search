package com.tech.challenge.flightsearch.modules.airport.controller;

import com.tech.challenge.flightsearch.models.dto.AirportDTO;
import com.tech.challenge.flightsearch.modules.airport.service.AirportService;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.UserRequestPostProcessor;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.testcontainers.shaded.com.fasterxml.jackson.core.JsonProcessingException;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.JsonNode;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;

import static org.hamcrest.Matchers.containsInRelativeOrder;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class AirportControllerTest {

    private static final UserRequestPostProcessor USER_ADMIN = user("teste").password("1234").roles("USER", "ADMIN");
    private static final UserRequestPostProcessor USER_AIRPORT = user("teste").password("1234").roles("USER", "AIRPORT");
    private static final UserRequestPostProcessor USER_CLIENT = user("teste").password("1234").roles("USER", "CLIENT");

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private AirportService airportService;

    @Test
    @Order(1)
    @DisplayName("List all airports unauthorized")
    void getAllAirports_unauthorizedError() throws Exception {
        mockMvc.perform(get("/airports")).andExpect(status().isUnauthorized());
    }

    @Test
    @Order(2)
    @DisplayName("List all airports forbidden")
    void getAllAirports_forbiddenError() throws Exception {
        mockMvc.perform(get("/airports").with(USER_CLIENT)).andExpect(status().isForbidden());
    }

    @Test
    @Order(3)
    @DisplayName("List all airports by admin user")
    void getAllAirports_byAdmin() throws Exception {
        MvcResult resultAdmin = mockMvc.perform(get("/airports")
                        .with(USER_ADMIN)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andReturn();
        resultAdmin.getAsyncResult();
        mockMvc.perform(asyncDispatch(resultAdmin))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$[*].code").value(containsInRelativeOrder("FRPCG", "BRSPG", "PTPHD")));
    }

    @Test
    @Order(4)
    @DisplayName("List all airports by airport user")
    void getAllAirports_byAirport() throws Exception {
        MvcResult resultAirport = mockMvc.perform(get("/airports")
                        .with(USER_AIRPORT).accept(MediaType.APPLICATION_JSON_VALUE))
                .andReturn();
        resultAirport.getAsyncResult();
        mockMvc.perform(asyncDispatch(resultAirport))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$[*].code").value(containsInRelativeOrder("FRPCG", "BRSPG", "PTPHD")));
    }

    @Test
    @Order(5)
    @DisplayName("Search all airports by airport filters")
    void searchAirports_byTerm() throws Exception {
        MvcResult resultAirport = mockMvc.perform(get("/airports/search")
                .param("term", "paris")
                .with(USER_AIRPORT).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        resultAirport.getAsyncResult();
        mockMvc.perform(asyncDispatch(resultAirport))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$.content").isArray())
                .andExpect(jsonPath("$.content[0].code").value("FRPCG"));
    }

    @Test
    @Order(6)
    @DisplayName("Search airports by code and by id")
    void findByCode_ById() throws Exception {
        MvcResult resultCode = mockMvc.perform(get("/airports/by-code/" + "FRPCG")
                .with(USER_AIRPORT).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        resultCode.getAsyncResult();
        mockMvc.perform(asyncDispatch(resultCode))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$.code").value("FRPCG"))
                .andExpect(jsonPath("$.name").value("Charles de Gaulle"));

        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode obj = objectMapper.readTree(resultCode.getResponse().getContentAsString());
        MvcResult resultById = mockMvc.perform(get("/airports/" + obj.get("id").textValue())
                .with(USER_AIRPORT).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        resultById.getAsyncResult();
        mockMvc.perform(asyncDispatch(resultById))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$.code").value("FRPCG"));
    }

    @Test
    @Order(7)
    @DisplayName("Create airports")
    void create() throws Exception {
        MvcResult resultCode = mockMvc.perform(post("/airports")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(getJson(true))
                        .with(USER_ADMIN)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andReturn();
        resultCode.getAsyncResult();
        mockMvc.perform(asyncDispatch(resultCode))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$.id").isNotEmpty())
                .andExpect(jsonPath("$.code").value("CODE"))
                .andExpect(jsonPath("$.name").value("Test"));
    }

    @Test
    @Order(8)
    @DisplayName("Update TEST airports")
    void update() throws Exception {
        MvcResult resultCode = mockMvc.perform(put("/airports")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(getJson(false))
                        .with(USER_ADMIN)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andReturn();
        resultCode.getAsyncResult();
        mockMvc.perform(asyncDispatch(resultCode))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$.id").isNotEmpty())
                .andExpect(jsonPath("$.code").value("CODE"))
                .andExpect(jsonPath("$.active").value(false))
                .andExpect(jsonPath("$.name").value("Test Update"));
    }

    @Test
    @Order(9)
    @DisplayName("Delete TEST airports")
    void delete() throws Exception {
        AirportDTO airportDTO = getAirportDTO(false);
        mockMvc.perform(MockMvcRequestBuilders.delete("/airports/{id}", airportDTO.getId())
                        .with(USER_ADMIN)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    private AirportDTO getAirportDTO(boolean isNew) {
        AirportDTO airport;
        if (isNew) {
            airport = AirportDTO.builder()
                    .name("Test")
                    .city("City")
                    .active(true)
                    .country("CT")
                    .code("CODE")
                    .build();
        } else {
            airport = airportService.findFirstByCode("CODE").block();
            assert airport != null;
            airport.setActive(false);
            airport.setName("Test Update");
        }

        return airport;
    }

    private String getJson(boolean isNew) throws JsonProcessingException {
        AirportDTO airport = getAirportDTO(isNew);
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(airport);
    }

}
