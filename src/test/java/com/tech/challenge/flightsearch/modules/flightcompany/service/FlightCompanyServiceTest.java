package com.tech.challenge.flightsearch.modules.flightcompany.service;

import com.tech.challenge.flightsearch.database.entity.FlightCompany;
import com.tech.challenge.flightsearch.database.repository.FlightCompanyRepository;
import com.tech.challenge.flightsearch.models.dto.FlightCompanyDTO;
import com.tech.challenge.flightsearch.models.mappers.FlightCompanyMapper;
import com.tech.challenge.flightsearch.models.mappers.FlightCompanyMapperImpl;
import com.tech.challenge.flightsearch.modules.flightcompany.provider.FlightCompanyProvider;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import reactor.test.StepVerifier;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;

public class FlightCompanyServiceTest {

    private final FlightCompanyRepository flightCompanyRepository = Mockito.mock(FlightCompanyRepository.class, Mockito.RETURNS_MOCKS);
    private final FlightCompanyMapper flightCompanyMapper = Mockito.mock(FlightCompanyMapperImpl.class, Mockito.CALLS_REAL_METHODS);
    private final FlightCompanyProvider flightCompanyProvider = new FlightCompanyProvider(flightCompanyRepository);
    private final FlightCompanyService flightCompanyService = new FlightCompanyService(flightCompanyProvider, flightCompanyMapper);

    private static final UUID uuid = UUID.randomUUID();

    private static FlightCompany getFlightCompany() {
        return FlightCompany.builder().id(uuid).name("TEST").code("1234").active(true).build();
    }

    private static FlightCompanyDTO getFlightCompanyDTO() {
        return FlightCompanyDTO.builder().id(uuid.toString()).name("TEST").code("1234").active(true).build();
    }

    @Test
    void findAll() {
        Mockito.when(flightCompanyRepository.findAll(Sort.by(Sort.Direction.ASC, "name"))).thenReturn(List.of(getFlightCompany()));
        StepVerifier.create(flightCompanyService.findAll())
                .recordWith(ArrayList::new)
                .expectNextCount(1L)
                .consumeRecordedWith(it -> {
                    assertEquals(1, it.size());
                    assertTrue(it.stream().findFirst().isPresent());
                })
                .verifyComplete();
    }

    @Test
    void findById() {
        Mockito.when(flightCompanyRepository.findById(uuid)).thenReturn(Optional.of(getFlightCompany()));
        StepVerifier.create(flightCompanyService.findById(uuid.toString()))
                .consumeNextWith(it ->assertEquals(uuid.toString(), it.getId()))
                .verifyComplete();
    }

    @Test
    void save() {
        final var flightCompanyCaptor = ArgumentCaptor.forClass(FlightCompany.class);
        FlightCompanyDTO flightCompanyDTO = getFlightCompanyDTO();
        flightCompanyDTO.setId(null);
        FlightCompany flightCompany = getFlightCompany();
        Mockito.when(flightCompanyRepository.save(flightCompanyCaptor.capture())).thenReturn(flightCompany);
        StepVerifier.create(flightCompanyService.save(flightCompanyDTO))
                .consumeNextWith(it -> {
                    final var savedFlightCompany = flightCompanyCaptor.getValue();
                    assertNotNull(it.getId());
                    assertEquals(savedFlightCompany.getCode(), it.getCode());
                })
                .verifyComplete();
    }

    @Test
    void update() {
        final var flightCompanyCaptor = ArgumentCaptor.forClass(FlightCompany.class);
        FlightCompany flightCompany = getFlightCompany();
        FlightCompanyDTO flightCompanyDTO = getFlightCompanyDTO();

        Mockito.when(flightCompanyRepository.save(flightCompanyCaptor.capture())).thenReturn(flightCompany);
        StepVerifier.create(flightCompanyService.update(flightCompanyDTO))
                .consumeNextWith(it -> {
                    final var savedFlightCompany = flightCompanyCaptor.getValue();
                    assertEquals(savedFlightCompany.getId().toString(), it.getId());
                    assertEquals(savedFlightCompany.getCode(), it.getCode());
                })
                .verifyComplete();
    }

    @Test
    void delete() {
        final var uuidSlot = ArgumentCaptor.forClass(UUID.class);
        Mockito.when(flightCompanyRepository.existsById(uuidSlot.capture())).thenReturn(true);
        Mockito.doNothing().when(flightCompanyRepository).deleteById(uuidSlot.capture());
        StepVerifier.create(flightCompanyService.delete(uuid.toString())).verifyComplete();
        assertEquals(uuid, uuidSlot.getValue());
    }

    @Test
    void findFirstByCode() {
        FlightCompany flightCompany = getFlightCompany();
        Mockito.when(flightCompanyRepository.findFirstByCodeIgnoreCase(flightCompany.getCode())).thenReturn(Optional.of(flightCompany));
        StepVerifier.create(flightCompanyService.findFirstByCode(flightCompany.getCode()))
                .consumeNextWith(it -> {
                    assertEquals(flightCompany.getId().toString(), it.getId());
                    assertEquals(flightCompany.getCode(), it.getCode());
                })
                .verifyComplete();
    }

    @Test
    void search() {
        Mockito.when(flightCompanyRepository.findAll((Specification<FlightCompany>) any(), (Pageable) any())).thenReturn(new PageImpl<>(List.of(getFlightCompany(), getFlightCompany())));
        PageRequest pageRequest = PageRequest.of(0, 10, Sort.by(Sort.Direction.valueOf("ASC"), "name"));
        StepVerifier.create(flightCompanyService.search("TES", true, pageRequest))
                .consumeNextWith(it -> {
                    assertEquals(2L, it.getTotalElements());
                    assertTrue(it.stream().findFirst().isPresent());
                })
                .verifyComplete();
    }

}
