package com.tech.challenge.flightsearch.models.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.annotation.Nonnull;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FlightRequestDTO {
    @JsonProperty("id")
    String id;
    @NotBlank(message = "code must be informed")
    @JsonProperty("code")
    String code;
    @NotNull(message = "price must be informed")
    @JsonProperty("price")
    BigDecimal price;
    @NotNull(message = "active must be informed")
    @JsonProperty("active")
    Boolean active;
    @NotNull(message = "flightCompanyId must be informed")
    @JsonProperty("flightCompanyId")
    String flightCompanyId;
    @NotNull(message = "from_airport_id must be informed")
    @JsonProperty("from_airport_id")
    String fromAirportId;
    @NotNull(message = "to_airport_id must be informed")
    @JsonProperty("to_airport_id")
    String toAirportId;

}
