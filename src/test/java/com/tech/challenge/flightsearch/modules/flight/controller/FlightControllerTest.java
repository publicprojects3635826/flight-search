package com.tech.challenge.flightsearch.modules.flight.controller;

import com.tech.challenge.flightsearch.models.dto.FlightDTO;
import com.tech.challenge.flightsearch.models.dto.FlightRequestDTO;
import com.tech.challenge.flightsearch.modules.flight.service.FlightService;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.UserRequestPostProcessor;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.testcontainers.shaded.com.fasterxml.jackson.core.JsonProcessingException;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.JsonNode;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;

import java.math.BigDecimal;

import static org.hamcrest.Matchers.containsInRelativeOrder;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class FlightControllerTest {

    private static final UserRequestPostProcessor USER_ADMIN = user("teste").password("1234").roles("USER", "ADMIN");
    private static final UserRequestPostProcessor USER_AIRPORT = user("teste").password("1234").roles("USER", "AIRPORT");
    private static final UserRequestPostProcessor USER_CLIENT = user("teste").password("1234").roles("USER", "CLIENT");
    public static final String CODE_0 = "CODE0";


    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private FlightService flightService;

    @Test
    @Order(1)
    @DisplayName("List all flight unauthorized")
    void getAllFlight_unauthorizedError() throws Exception {
        mockMvc.perform(get("/flight")).andExpect(status().isUnauthorized());
    }

    @Test
    @Order(2)
    @DisplayName("List all flight forbidden")
    void getAllFlight_forbiddenError() throws Exception {
        mockMvc.perform(get("/flight").with(USER_CLIENT)).andExpect(status().isForbidden());
    }

    @Test
    @Order(3)
    @DisplayName("List all flight by admin user")
    void getAllFlight_byAdmin() throws Exception {
        MvcResult resultAdmin = mockMvc.perform(get("/flight")
                        .with(USER_ADMIN)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andReturn();
        resultAdmin.getAsyncResult();
        mockMvc.perform(asyncDispatch(resultAdmin))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$[*].code").value(containsInRelativeOrder("PTFR123", "FRBR123", "BRPT123")));
    }

    @Test
    @Order(4)
    @DisplayName("List all flight by flight user")
    void getAllFlight_byFlight() throws Exception {
        MvcResult resultFlight = mockMvc.perform(get("/flight")
                        .with(USER_AIRPORT).accept(MediaType.APPLICATION_JSON_VALUE))
                .andReturn();
        resultFlight.getAsyncResult();
        mockMvc.perform(asyncDispatch(resultFlight))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$[*].code").value(containsInRelativeOrder("PTFR123", "FRBR123", "BRPT123")));
    }

    @Test
    @Order(5)
    @DisplayName("Search all flight by flight filters")
    void searchFlight_byTerm() throws Exception {
        MvcResult resultFlight = mockMvc.perform(get("/flight/search")
                .param("term", "paris")
                .with(USER_AIRPORT).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        resultFlight.getAsyncResult();
        mockMvc.perform(asyncDispatch(resultFlight))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$.content").isArray())
                .andExpect(jsonPath("$.content[0].code").value("PTFR123"));
    }

    @Test
    @Order(6)
    @DisplayName("Search flight by code and by id")
    void findByCode_ById() throws Exception {
        MvcResult resultCode = mockMvc.perform(get("/flight/by-code/" + "PTFR123")
                .with(USER_AIRPORT).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        resultCode.getAsyncResult();
        mockMvc.perform(asyncDispatch(resultCode))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$.code").value("PTFR123"));

        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode obj = objectMapper.readTree(resultCode.getResponse().getContentAsString());
        MvcResult resultById = mockMvc.perform(get("/flight/" + obj.get("id").textValue())
                .with(USER_AIRPORT).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        resultById.getAsyncResult();
        mockMvc.perform(asyncDispatch(resultById))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$.code").value("PTFR123"));
    }

    @Test
    @Order(7)
    @DisplayName("Create flight")
    void create() throws Exception {
        MvcResult resultCode = mockMvc.perform(post("/flight")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(getJson(true))
                        .with(USER_ADMIN)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andReturn();
        resultCode.getAsyncResult();
        mockMvc.perform(asyncDispatch(resultCode))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$.id").isNotEmpty())
                .andExpect(jsonPath("$.code").value(CODE_0))
                .andExpect(jsonPath("$.price").value(BigDecimal.ONE));
    }

    @Test
    @Order(8)
    @DisplayName("Update TEST flight")
    void update() throws Exception {
        MvcResult resultCode = mockMvc.perform(put("/flight")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(getJson(false))
                        .with(USER_ADMIN)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andReturn();
        resultCode.getAsyncResult();
        mockMvc.perform(asyncDispatch(resultCode))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$.id").isNotEmpty())
                .andExpect(jsonPath("$.code").value(CODE_0))
                .andExpect(jsonPath("$.active").value(false))
                .andExpect(jsonPath("$.price").value(BigDecimal.TEN));
    }

    @Test
    @Order(9)
    @DisplayName("Delete TEST flight")
    void delete() throws Exception {
        FlightRequestDTO flightRequestDTO = getFlightRequestDTO(false);
        mockMvc.perform(MockMvcRequestBuilders.delete("/flight/{id}", flightRequestDTO.getId())
                        .with(USER_ADMIN)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    @Order(10)
    @DisplayName("Search all flight by from/to")
    void searchFlight_bestFlight() throws Exception {
        MvcResult resultFlight = mockMvc.perform(get("/flight/search-best-price")
                .param("from", "paris")
                .param("to", "São")
                .with(USER_AIRPORT).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        resultFlight.getAsyncResult();
        mockMvc.perform(asyncDispatch(resultFlight))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$.content").isArray())
                .andExpect(jsonPath("$.content[0].id").value("cc9dc331-5bc0-4d67-8abc-b90cf2b8f085"))
                .andExpect(jsonPath("$.content[0].code").value("FRBR123"))
                .andExpect(jsonPath("$.content[0].price").value("1543.34"));
    }

    private FlightRequestDTO getFlightRequestDTO(boolean isNew) {
        FlightRequestDTO.FlightRequestDTOBuilder builder = FlightRequestDTO.builder();
        if (isNew) {
            FlightDTO flight = flightService.findAll().blockFirst();
            assert flight != null;
            builder.code(CODE_0);
            builder.active(true);
            builder.price(BigDecimal.ONE);
            builder.flightCompanyId(flight.getFlightCompany().getId());
            builder.fromAirportId(flight.getToAirport().getId()).toAirportId(flight.getFromAirport().getId());
        } else {
            FlightDTO flight = flightService.findFirstByCode(CODE_0).block();
            assert flight != null;
            builder.id(flight.getId());
            builder.code(CODE_0);
            builder.active(false);
            builder.price(BigDecimal.TEN);
            builder.flightCompanyId(flight.getFlightCompany().getId());
            builder.fromAirportId(flight.getToAirport().getId()).toAirportId(flight.getFromAirport().getId());
        }

        return builder.build();
    }

    private String getJson(boolean isNew) throws JsonProcessingException {
        FlightRequestDTO flight = getFlightRequestDTO(isNew);
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(flight);
    }

}
