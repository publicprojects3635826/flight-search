package com.tech.challenge.flightsearch.models.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;

@Getter
@RequiredArgsConstructor
public enum ServiceError {

    AIRPORT_EXIST(HttpStatus.BAD_REQUEST, "airport code already exists"),
    AIRPORT_NOT_FOUND(HttpStatus.NOT_FOUND, "airport not found"),
    AIRPORT_ID_NOT_EMPTY(HttpStatus.BAD_REQUEST, "airport id not empty"),
    AIRPORT_ID_EMPTY(HttpStatus.BAD_REQUEST, "airport id is empty"),
    FLIGHT_COMPANY_EXIST(HttpStatus.BAD_REQUEST, "flight company code already exists"),
    FLIGHT_COMPANY_NOT_FOUND(HttpStatus.NOT_FOUND, "flight company not found"),
    FLIGHT_COMPANY_ID_NOT_EMPTY(HttpStatus.BAD_REQUEST, "flight company id not empty"),
    FLIGHT_COMPANY_ID_EMPTY(HttpStatus.BAD_REQUEST, "flight company id is empty"),
    FLIGHT_EXIST(HttpStatus.BAD_REQUEST, "flight code already exists"),
    FLIGHT_NOT_FOUND(HttpStatus.NOT_FOUND, "flight not found"),
    FLIGHT_ID_NOT_EMPTY(HttpStatus.BAD_REQUEST, "flight id not empty"),
    FLIGHT_ID_EMPTY(HttpStatus.BAD_REQUEST, "flight id is empty"),
    BAD_REQUEST(HttpStatus.BAD_REQUEST, "invalid data"),
    INTERNAL_SERVER_ERROR(HttpStatus.INTERNAL_SERVER_ERROR, "internal error contact admin"),
    INVALID_ACCESS(HttpStatus.FORBIDDEN, "invalid permissions");

    private final HttpStatus status;
    private final String message;

}
