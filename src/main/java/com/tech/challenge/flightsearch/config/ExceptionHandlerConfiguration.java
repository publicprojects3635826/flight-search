package com.tech.challenge.flightsearch.config;

import com.tech.challenge.flightsearch.models.enums.ServiceError;
import com.tech.challenge.flightsearch.models.error.ServiceException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authorization.AuthorizationDeniedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
public class ExceptionHandlerConfiguration extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = ServiceException.class)
    protected ResponseEntity<Object> handleServiceException(RuntimeException ex, WebRequest request) {
        ServiceException serviceException = (ServiceException) ex;
        HttpStatus status = serviceException.getStatus();
        Map<String, Object> body = getResponseEntityBody(serviceException.getMessage(), status);
        return handleExceptionInternal(ex, body, new HttpHeaders(), status, request);
    }

    @ExceptionHandler(value = AuthorizationDeniedException.class)
    protected ResponseEntity<Object> handleAccessDeniedException(RuntimeException ex, WebRequest request) {
        ServiceError internalServerError = ServiceError.INVALID_ACCESS;
        HttpStatus status = internalServerError.getStatus();
        Map<String, Object> body = getResponseEntityBody(internalServerError.getMessage(), status);
        return handleExceptionInternal(ex, body, new HttpHeaders(), status, request);
    }

    @ExceptionHandler(value = Exception.class)
    protected ResponseEntity<Object> handleException(RuntimeException ex, WebRequest request) {
        ServiceError internalServerError = ServiceError.INTERNAL_SERVER_ERROR;
        HttpStatus status = internalServerError.getStatus();
        Map<String, Object> body = getResponseEntityBody(internalServerError.getMessage(), status);
        return handleExceptionInternal(ex, body, new HttpHeaders(), status, request);
    }

    private Map<String, Object> getResponseEntityBody(String msg, HttpStatus status) {
        Map<String, Object> body = new HashMap<>();
        body.put("message", msg);
        body.put("status", status);
        body.put("statusCode", status.value());
        return body;
    }

}
