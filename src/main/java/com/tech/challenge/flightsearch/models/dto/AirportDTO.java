package com.tech.challenge.flightsearch.models.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AirportDTO {
    @JsonProperty("id")
    String id;
    @NotBlank(message = "name must be informed")
    @JsonProperty("name")
    String name;
    @NotBlank(message = "code must be informed")
    @JsonProperty("code")
    String code;
    @NotBlank(message = "country must be informed")
    @JsonProperty("country")
    String country;
    @NotBlank(message = "city must be informed")
    @JsonProperty("city")
    String city;
    @JsonProperty("active")
    Boolean active;
}
