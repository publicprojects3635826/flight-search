package com.tech.challenge.flightsearch.database.specifications;

import com.tech.challenge.flightsearch.database.entity.Airport;
import io.micrometer.common.util.StringUtils;
import org.springframework.data.jpa.domain.Specification;

public class AirportSpecification {

    public Specification<Airport> search(String term) {
        final var search = StringUtils.isBlank(term) ? "" : term;
        return (root, query, criteriaBuilder) -> criteriaBuilder.or(
                criteriaBuilder.like(criteriaBuilder.lower(root.get("name")), "%" + search.toLowerCase() + "%"),
                criteriaBuilder.like(criteriaBuilder.lower(root.get("code")), "%" + search.toLowerCase() + "%"),
                criteriaBuilder.like(criteriaBuilder.lower(root.get("country")), "%" + search.toLowerCase() + "%"),
                criteriaBuilder.like(criteriaBuilder.lower(root.get("city")), "%" + search.toLowerCase() + "%")
        );
    }

    public Specification<Airport> active(boolean active) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("active"), active);
    }

}
