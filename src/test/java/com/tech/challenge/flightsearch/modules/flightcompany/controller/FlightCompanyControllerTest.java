package com.tech.challenge.flightsearch.modules.flightcompany.controller;

import com.tech.challenge.flightsearch.models.dto.FlightCompanyDTO;
import com.tech.challenge.flightsearch.modules.flightcompany.service.FlightCompanyService;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.UserRequestPostProcessor;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.testcontainers.shaded.com.fasterxml.jackson.core.JsonProcessingException;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.JsonNode;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;

import static org.hamcrest.Matchers.containsInRelativeOrder;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class FlightCompanyControllerTest {

    private static final UserRequestPostProcessor USER_ADMIN = user("teste").password("1234").roles("USER", "ADMIN");
    private static final UserRequestPostProcessor USER_AIRPORT = user("teste").password("1234").roles("USER", "AIRPORT");
    private static final UserRequestPostProcessor USER_CLIENT = user("teste").password("1234").roles("USER", "CLIENT");

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private FlightCompanyService flightCompaniesService;

    @Test
    @Order(1)
    @DisplayName("List all flight companies unauthorized")
    void getAllFlightCompanies_unauthorizedError() throws Exception {
        mockMvc.perform(get("/flight-company")).andExpect(status().isUnauthorized());
    }

    @Test
    @Order(2)
    @DisplayName("List all flight companies forbidden")
    void getAllFlightCompanies_forbiddenError() throws Exception {
        mockMvc.perform(get("/flight-company").with(USER_CLIENT)).andExpect(status().isForbidden());
    }

    @Test
    @Order(3)
    @DisplayName("List all flight companies by admin user")
    void getAllFlightCompanies_byAdmin() throws Exception {
        MvcResult resultAdmin = mockMvc.perform(get("/flight-company")
                        .with(USER_ADMIN)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andReturn();
        resultAdmin.getAsyncResult();
        mockMvc.perform(asyncDispatch(resultAdmin))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$[*].code").value(containsInRelativeOrder("F123", "L123", "T123")));
    }

    @Test
    @Order(4)
    @DisplayName("List all flight companies by flightCompanies user")
    void getAllFlightCompanies_byFlightCompany() throws Exception {
        MvcResult resultFlightCompany = mockMvc.perform(get("/flight-company")
                        .with(USER_AIRPORT).accept(MediaType.APPLICATION_JSON_VALUE))
                .andReturn();
        resultFlightCompany.getAsyncResult();
        mockMvc.perform(asyncDispatch(resultFlightCompany))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$[*].code").value(containsInRelativeOrder("F123", "L123", "T123")));
    }

    @Test
    @Order(5)
    @DisplayName("Search all flight companies by flightCompanies filters")
    void searchFlightCompanies_byTerm() throws Exception {
        MvcResult resultFlightCompany = mockMvc.perform(get("/flight-company/search")
                .param("term", "gua")
                .with(USER_AIRPORT).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        resultFlightCompany.getAsyncResult();
        mockMvc.perform(asyncDispatch(resultFlightCompany))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$.content").isArray())
                .andExpect(jsonPath("$.content[0].code").value("L123"));
    }

    @Test
    @Order(6)
    @DisplayName("Search flight companies by code and by id")
    void findByCode_ById() throws Exception {
        MvcResult resultCode = mockMvc.perform(get("/flight-company/by-code/" + "F123")
                .with(USER_AIRPORT).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        resultCode.getAsyncResult();
        mockMvc.perform(asyncDispatch(resultCode))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$.code").value("F123"))
                .andExpect(jsonPath("$.name").value("AirFrance"));

        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode obj = objectMapper.readTree(resultCode.getResponse().getContentAsString());
        MvcResult resultById = mockMvc.perform(get("/flight-company/" + obj.get("id").textValue())
                .with(USER_AIRPORT).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        resultById.getAsyncResult();
        mockMvc.perform(asyncDispatch(resultById))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$.code").value("F123"));
    }

    @Test
    @Order(7)
    @DisplayName("Create flight companues")
    void create() throws Exception {
        MvcResult resultCode = mockMvc.perform(post("/flight-company")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(getJson(true))
                        .with(USER_ADMIN)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andReturn();
        resultCode.getAsyncResult();
        mockMvc.perform(asyncDispatch(resultCode))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$.id").isNotEmpty())
                .andExpect(jsonPath("$.code").value("CODE"))
                .andExpect(jsonPath("$.name").value("Test"));
    }

    @Test
    @Order(8)
    @DisplayName("Update TEST flight companues")
    void update() throws Exception {
        MvcResult resultCode = mockMvc.perform(put("/flight-company")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(getJson(false))
                        .with(USER_ADMIN)
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andReturn();
        resultCode.getAsyncResult();
        mockMvc.perform(asyncDispatch(resultCode))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$.id").isNotEmpty())
                .andExpect(jsonPath("$.code").value("CODE"))
                .andExpect(jsonPath("$.active").value(false))
                .andExpect(jsonPath("$.name").value("Test Update"));
    }

    @Test
    @Order(9)
    @DisplayName("Delete TEST flight companues")
    void delete() throws Exception {
        FlightCompanyDTO flightCompaniesDTO = getFlightCompanyDTO(false);
        mockMvc.perform(MockMvcRequestBuilders.delete("/flight-company/{id}", flightCompaniesDTO.getId())
                        .with(USER_ADMIN)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    private FlightCompanyDTO getFlightCompanyDTO(boolean isNew) {
        FlightCompanyDTO flightCompanies;
        if (isNew) {
            flightCompanies = FlightCompanyDTO.builder()
                    .name("Test")
                    .active(true)
                    .code("CODE")
                    .build();
        } else {
            flightCompanies = flightCompaniesService.findFirstByCode("CODE").block();
            assert flightCompanies != null;
            flightCompanies.setActive(false);
            flightCompanies.setName("Test Update");
        }

        return flightCompanies;
    }

    private String getJson(boolean isNew) throws JsonProcessingException {
        FlightCompanyDTO flightCompanies = getFlightCompanyDTO(isNew);
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(flightCompanies);
    }

}
