package com.tech.challenge.flightsearch.modules.flightcompany.service;

import com.tech.challenge.flightsearch.models.dto.FlightCompanyDTO;
import com.tech.challenge.flightsearch.models.enums.ServiceError;
import com.tech.challenge.flightsearch.models.error.ServiceException;
import com.tech.challenge.flightsearch.models.mappers.FlightCompanyMapper;
import com.tech.challenge.flightsearch.modules.flightcompany.provider.FlightCompanyProvider;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Objects;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class FlightCompanyService {

    private final FlightCompanyProvider flightCompanyProvider;
    private final FlightCompanyMapper flightCompanyMapper;

    public Flux<FlightCompanyDTO> findAll() {
        return Flux.fromIterable(flightCompanyProvider.findAll())
                .map(flightCompanyMapper::toDto);
    }

    public Mono<FlightCompanyDTO> findById(String id) {
        return Mono.fromCallable(() -> UUID.fromString(id))
                .onErrorMap(error -> new ServiceException(ServiceError.BAD_REQUEST))
                .mapNotNull(uuid -> flightCompanyProvider.findById(uuid).orElse(null))
                .switchIfEmpty(Mono.error(new ServiceException(ServiceError.FLIGHT_COMPANY_NOT_FOUND)))
                .map(flightCompanyMapper::toDto);
    }

    public Mono<FlightCompanyDTO> save(FlightCompanyDTO flightCompanyDTO) {
        return Mono.fromCallable(() -> flightCompanyMapper.toEntity(flightCompanyDTO))
                .onErrorMap(error -> new ServiceException(ServiceError.BAD_REQUEST))
                .doOnNext(flightCompany -> {
                    if (Objects.nonNull(flightCompany.getId()))
                        throw new ServiceException(ServiceError.FLIGHT_COMPANY_ID_NOT_EMPTY);
                    if (flightCompanyProvider.findFirstByCode(flightCompany.getCode()).isPresent())
                        throw new ServiceException(ServiceError.FLIGHT_COMPANY_EXIST);
                })
                .map(flightCompanyProvider::save)
                .map(flightCompanyMapper::toDto);

    }

    public Mono<FlightCompanyDTO> update(FlightCompanyDTO flightCompanyDTO) {
        return Mono.fromCallable(() -> flightCompanyMapper.toEntity(flightCompanyDTO))
                .onErrorMap(error -> new ServiceException(ServiceError.BAD_REQUEST))
                .doOnNext(flightCompany -> {
                    if (Objects.isNull(flightCompany.getId()))
                        throw new ServiceException(ServiceError.FLIGHT_COMPANY_ID_NOT_EMPTY);
                })
                .map(flightCompanyProvider::save)
                .map(flightCompanyMapper::toDto);

    }

    public Mono<Void> delete(String id) {
        return Mono.fromCallable(() -> UUID.fromString(id))
                .onErrorMap(error -> new ServiceException(ServiceError.BAD_REQUEST))
                .doOnNext(uuid -> {
                    if (!flightCompanyProvider.existsById(uuid))
                        throw new ServiceException(ServiceError.FLIGHT_COMPANY_NOT_FOUND);
                })
                .doOnNext(flightCompanyProvider::delete)
                .then();

    }

    public Mono<FlightCompanyDTO> findFirstByCode(String code) {
        return Mono.fromCallable(() -> flightCompanyProvider.findFirstByCode(code).orElse(null))
                .switchIfEmpty(Mono.error(new ServiceException(ServiceError.FLIGHT_COMPANY_NOT_FOUND)))
                .map(flightCompanyMapper::toDto);
    }

    public Mono<Page<FlightCompanyDTO>> search(String term, Boolean active, Pageable pageable) {
        return Mono.fromCallable(() -> flightCompanyProvider.search(term, active, pageable))
                .map(page -> page.map(flightCompanyMapper::toDto));
    }
}
