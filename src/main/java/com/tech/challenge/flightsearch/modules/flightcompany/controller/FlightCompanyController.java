package com.tech.challenge.flightsearch.modules.flightcompany.controller;

import com.tech.challenge.flightsearch.models.dto.FlightCompanyDTO;
import com.tech.challenge.flightsearch.models.enums.Permission;
import com.tech.challenge.flightsearch.modules.flightcompany.service.FlightCompanyService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Validated
@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@RequestMapping("/flight-company")
@Tag(name = "FlightCompany", description = "FlightCompany controller")
public class FlightCompanyController {

    private final FlightCompanyService flightCompanyService;

    @Secured(value = {Permission.Constants.ADMIN, Permission.Constants.AIRPORT})
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @Operation(
            description = "Search all flight companies",
            responses = {
                    @ApiResponse(responseCode = "200", description = "return list of flight companies",
                            content = {@Content(array = @ArraySchema(schema = @Schema(implementation = FlightCompanyDTO.class)))}
                    ),
                    @ApiResponse(responseCode = "401", description = "unauthorized"),
                    @ApiResponse(responseCode = "403", description = "forbidding"),
            }
    )
    public Flux<FlightCompanyDTO> findAll() {
        return flightCompanyService.findAll();
    }

    @Secured(value = {Permission.Constants.USER})
    @GetMapping(value = "search", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @Operation(
            description = "Search flightCompany by term",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Return flight companies paginated",
                            content = @Content(array = @ArraySchema(schema = @Schema(implementation = FlightCompanyDTO.class)))
                    ),
                    @ApiResponse(responseCode = "401", description = "unauthorized")
            }
    )
    public Mono<Page<FlightCompanyDTO>> search(
            @RequestParam(value = "term", required = false) String term,
            @RequestParam(name = "active", required = false) Boolean active,
            @RequestParam(value = "page", defaultValue = "0") Integer page,
            @RequestParam(value = "size", defaultValue = "10") Integer size,
            @RequestParam(value = "sort", defaultValue = "name") String sort,
            @RequestParam(value = "direction", defaultValue = "ASC") String direction
    ) {
        return flightCompanyService.search(term, active, PageRequest.of(page, size, Sort.by(Sort.Direction.valueOf(direction), sort)));
    }

    @Secured(value = {Permission.Constants.ADMIN, Permission.Constants.AIRPORT})
    @GetMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @Operation(
            description = "Search flightCompany by UUID",
            responses = {
                    @ApiResponse(responseCode = "200", description = "return flightCompany",
                            content = {@Content(array = @ArraySchema(schema = @Schema(implementation = FlightCompanyDTO.class)))}
                    ),
                    @ApiResponse(responseCode = "401", description = "unauthorized"),
                    @ApiResponse(responseCode = "403", description = "forbidding"),
                    @ApiResponse(responseCode = "404", description = "flightCompany not found"),
            }
    )
    public Mono<FlightCompanyDTO> findById(@PathVariable String id) {
        return flightCompanyService.findById(id);
    }

    @Secured(value = {Permission.Constants.USER})
    @GetMapping(value = "by-code/{code}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @Operation(
            description = "Search flightCompany by code",
            responses = {
                    @ApiResponse(responseCode = "200", description = "return flightCompany",
                            content = {@Content(array = @ArraySchema(schema = @Schema(implementation = FlightCompanyDTO.class)))}
                    ),
                    @ApiResponse(responseCode = "401", description = "unauthorized"),
                    @ApiResponse(responseCode = "404", description = "flightCompany not found"),
            }
    )
    public Mono<FlightCompanyDTO> findFirstByCode(@PathVariable() String code) {
        return flightCompanyService.findFirstByCode(code);
    }

    @Secured(value = {Permission.Constants.ADMIN})
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(
            description = "Create new flightCompany",
            responses = {
                    @ApiResponse(responseCode = "201", description = "return flightCompany",
                            content = @Content(schema = @Schema(implementation = FlightCompanyDTO.class))
                    ),
                    @ApiResponse(responseCode = "400", description = "flightCompany id not empty"),
                    @ApiResponse(responseCode = "401", description = "unauthorized"),
                    @ApiResponse(responseCode = "403", description = "forbidding")
            }
    )
    public Mono<FlightCompanyDTO> save(@Valid @RequestBody FlightCompanyDTO userDTO) {
        return flightCompanyService.save(userDTO);
    }

    @Secured(value = {Permission.Constants.ADMIN})
    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(
            description = "Update existing flightCompany",
            responses = {
                    @ApiResponse(responseCode = "201", description = "return flightCompany",
                            content = @Content(schema = @Schema(implementation = FlightCompanyDTO.class))
                    ),
                    @ApiResponse(responseCode = "400", description = "flight company id is empty"),
                    @ApiResponse(responseCode = "401", description = "unauthorized"),
                    @ApiResponse(responseCode = "403", description = "forbidding")
            }
    )
    public Mono<FlightCompanyDTO> update(@Valid @RequestBody FlightCompanyDTO userDTO) {
        return flightCompanyService.update(userDTO);
    }


    @Secured(value = {Permission.Constants.ADMIN})
    @DeleteMapping(value = "{id}")
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(
            description = "Delete existing flightCompany by ID",
            responses = {
                    @ApiResponse(responseCode = "201", description = "return flightCompany",
                            content = @Content(schema = @Schema(implementation = FlightCompanyDTO.class))
                    ),
                    @ApiResponse(responseCode = "400", description = "flightCompany id is empty"),
                    @ApiResponse(responseCode = "403", description = "forbidding")
            }
    )
    public Mono<Void> delete(@Valid @PathVariable String id) {
        return flightCompanyService.delete(id);
    }

}
