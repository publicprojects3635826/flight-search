package com.tech.challenge.flightsearch.modules.flight.service;

import com.tech.challenge.flightsearch.models.dto.FlightDTO;
import com.tech.challenge.flightsearch.models.dto.FlightRequestDTO;
import com.tech.challenge.flightsearch.models.enums.ServiceError;
import com.tech.challenge.flightsearch.models.error.ServiceException;
import com.tech.challenge.flightsearch.models.mappers.FlightMapper;
import com.tech.challenge.flightsearch.modules.airport.provider.AirportProvider;
import com.tech.challenge.flightsearch.modules.flight.provider.FlightProvider;
import com.tech.challenge.flightsearch.modules.flightcompany.provider.FlightCompanyProvider;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Objects;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class FlightService {

    private final FlightProvider flightProvider;
    private final FlightMapper flightMapper;
    private final FlightCompanyProvider flightCompanyProvider;
    private final AirportProvider airportProvider;

    public Flux<FlightDTO> findAll() {
        return Flux.fromIterable(flightProvider.findAll())
                .map(flightMapper::toDto);
    }

    public Mono<FlightDTO> findById(String id) {
        return Mono.fromCallable(() -> UUID.fromString(id))
                .onErrorMap(error -> new ServiceException(ServiceError.BAD_REQUEST))
                .mapNotNull(uuid -> flightProvider.findById(uuid).orElse(null))
                .switchIfEmpty(Mono.error(new ServiceException(ServiceError.FLIGHT_NOT_FOUND)))
                .map(flightMapper::toDto);
    }

    public Mono<FlightDTO> save(FlightRequestDTO flightDTO) {
        return Mono.fromCallable(() -> flightMapper.fromRequestDTO(flightDTO))
                .onErrorMap(error -> new ServiceException(ServiceError.BAD_REQUEST))
                .doOnNext(flight -> {
                    if (Objects.nonNull(flight.getId()))
                        throw new ServiceException(ServiceError.FLIGHT_ID_NOT_EMPTY);
                    if (flightProvider.findFirstByCode(flight.getCode()).isPresent())
                        throw new ServiceException(ServiceError.FLIGHT_EXIST);
                    if (!flightCompanyProvider.existsById(flight.getFlightCompany().getId()))
                        throw new ServiceException(ServiceError.FLIGHT_COMPANY_NOT_FOUND);
                    if (!airportProvider.existsById(flight.getFromAirport().getId()))
                        throw new ServiceException(ServiceError.AIRPORT_NOT_FOUND);
                    if (!airportProvider.existsById(flight.getToAirport().getId()))
                        throw new ServiceException(ServiceError.AIRPORT_NOT_FOUND);
                })
                .map(flightProvider::save)
                .map(flightMapper::toDto);

    }

    public Mono<FlightDTO> update(FlightRequestDTO flightDTO) {
        return Mono.fromCallable(() -> flightMapper.fromRequestDTO(flightDTO))
                .onErrorMap(error -> new ServiceException(ServiceError.BAD_REQUEST))
                .doOnNext(flight -> {
                    if (Objects.isNull(flight.getId()))
                        throw new ServiceException(ServiceError.FLIGHT_ID_NOT_EMPTY);
                    if (!airportProvider.existsById(flight.getFromAirport().getId()))
                        throw new ServiceException(ServiceError.AIRPORT_NOT_FOUND);
                    if (!airportProvider.existsById(flight.getToAirport().getId()))
                        throw new ServiceException(ServiceError.AIRPORT_NOT_FOUND);
                    if (!flightCompanyProvider.existsById(flight.getFlightCompany().getId()))
                        throw new ServiceException(ServiceError.FLIGHT_COMPANY_NOT_FOUND);
                })
                .map(flightProvider::save)
                .map(flightMapper::toDto);

    }

    public Mono<Void> delete(String id) {
        return Mono.fromCallable(() -> UUID.fromString(id))
                .onErrorMap(error -> new ServiceException(ServiceError.BAD_REQUEST))
                .doOnNext(uuid -> {
                    if (!flightProvider.existsById(uuid))
                        throw new ServiceException(ServiceError.FLIGHT_NOT_FOUND);
                })
                .doOnNext(flightProvider::delete)
                .then();

    }

    public Mono<FlightDTO> findFirstByCode(String code) {
        return Mono.fromCallable(() -> flightProvider.findFirstByCode(code).orElse(null))
                .switchIfEmpty(Mono.error(new ServiceException(ServiceError.FLIGHT_NOT_FOUND)))
                .map(flightMapper::toDto);
    }

    public Mono<Page<FlightDTO>> search(String term, Boolean active, Pageable pageable) {
        return Mono.fromCallable(() -> flightProvider.search(term, active, pageable))
                .map(page -> page.map(flightMapper::toDto));
    }

    public Mono<Page<FlightDTO>> searchBestPrice(String from, String to, Pageable pageable) {
        return Mono.fromCallable(() -> flightProvider.searchBestPrice(from, to, pageable))
                .map(page -> page.map(flightMapper::toDto));
    }
}
