package com.tech.challenge.flightsearch.modules.airport.controller;

import com.tech.challenge.flightsearch.models.dto.AirportDTO;
import com.tech.challenge.flightsearch.models.enums.Permission;
import com.tech.challenge.flightsearch.modules.airport.service.AirportService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Validated
@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@RequestMapping("/airports")
@Tag(name = "Airport", description = "Airport controller")
public class AirportController {

    private final AirportService airportService;

    @Secured(value = {Permission.Constants.ADMIN, Permission.Constants.AIRPORT})
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @Operation(
            description = "Search all airports",
            responses = {
                    @ApiResponse(responseCode = "200", description = "return list of airports",
                            content = {@Content(array = @ArraySchema(schema = @Schema(implementation = AirportDTO.class)))}
                    ),
                    @ApiResponse(responseCode = "401", description = "unauthorized"),
                    @ApiResponse(responseCode = "403", description = "forbidding"),
            }
    )
    public Flux<AirportDTO> findAll() {
        return airportService.findAll();
    }

    @Secured(value = {Permission.Constants.USER})
    @GetMapping(value = "search", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @Operation(
            description = "Search airport by term",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Return airports paginated",
                            content = @Content(array = @ArraySchema(schema = @Schema(implementation = AirportDTO.class)))
                    ),
                    @ApiResponse(responseCode = "401", description = "unauthorized")
            }
    )
    public Mono<Page<AirportDTO>> search(
            @RequestParam(value = "term", required = false) String term,
            @RequestParam(name = "active", required = false) Boolean active,
            @RequestParam(value = "page", defaultValue = "0") Integer page,
            @RequestParam(value = "size", defaultValue = "10") Integer size,
            @RequestParam(value = "sort", defaultValue = "name") String sort,
            @RequestParam(value = "direction", defaultValue = "ASC") String direction
    ) {
        return airportService.search(term, active, PageRequest.of(page, size, Sort.by(Sort.Direction.valueOf(direction), sort)));
    }

    @Secured(value = {Permission.Constants.ADMIN, Permission.Constants.AIRPORT})
    @GetMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @Operation(
            description = "Search airport by UUID",
            responses = {
                    @ApiResponse(responseCode = "200", description = "return airport",
                            content = {@Content(array = @ArraySchema(schema = @Schema(implementation = AirportDTO.class)))}
                    ),
                    @ApiResponse(responseCode = "401", description = "unauthorized"),
                    @ApiResponse(responseCode = "403", description = "forbidding"),
                    @ApiResponse(responseCode = "404", description = "airport not found"),
            }
    )
    public Mono<AirportDTO> findById(@PathVariable String id) {
        return airportService.findById(id);
    }

    @Secured(value = {Permission.Constants.USER})
    @GetMapping(value = "by-code/{code}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @Operation(
            description = "Search airport by code",
            responses = {
                    @ApiResponse(responseCode = "200", description = "return airport",
                            content = {@Content(array = @ArraySchema(schema = @Schema(implementation = AirportDTO.class)))}
                    ),
                    @ApiResponse(responseCode = "401", description = "unauthorized"),
                    @ApiResponse(responseCode = "404", description = "airport not found"),
            }
    )
    public Mono<AirportDTO> findFirstByCode(@PathVariable() String code) {
        return airportService.findFirstByCode(code);
    }

    @Secured(value = {Permission.Constants.ADMIN})
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(
            description = "Create new airport",
            responses = {
                    @ApiResponse(responseCode = "201", description = "return airport",
                            content = @Content(schema = @Schema(implementation = AirportDTO.class))
                    ),
                    @ApiResponse(responseCode = "400", description = "airport id not empty"),
                    @ApiResponse(responseCode = "401", description = "unauthorized"),
                    @ApiResponse(responseCode = "403", description = "forbidding")
            }
    )
    public Mono<AirportDTO> save(@Valid @RequestBody AirportDTO userDTO) {
        return airportService.save(userDTO);
    }

    @Secured(value = {Permission.Constants.ADMIN})
    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(
            description = "Update existing airport",
            responses = {
                    @ApiResponse(responseCode = "201", description = "return airport",
                            content = @Content(schema = @Schema(implementation = AirportDTO.class))
                    ),
                    @ApiResponse(responseCode = "400", description = "airport id is empty"),
                    @ApiResponse(responseCode = "401", description = "unauthorized"),
                    @ApiResponse(responseCode = "403", description = "forbidding")
            }
    )
    public Mono<AirportDTO> update(@Valid @RequestBody AirportDTO userDTO) {
        return airportService.update(userDTO);
    }


    @Secured(value = {Permission.Constants.ADMIN})
    @DeleteMapping(value = "{id}")
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(
            description = "Delete existing airport by ID",
            responses = {
                    @ApiResponse(responseCode = "201", description = "return airport",
                            content = @Content(schema = @Schema(implementation = AirportDTO.class))
                    ),
                    @ApiResponse(responseCode = "400", description = "airport id is empty"),
                    @ApiResponse(responseCode = "403", description = "forbidding")
            }
    )
    public Mono<Void> delete(@Valid @PathVariable String id) {
        return airportService.delete(id);
    }

}
