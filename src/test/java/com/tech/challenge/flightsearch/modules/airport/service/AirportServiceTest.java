package com.tech.challenge.flightsearch.modules.airport.service;

import com.tech.challenge.flightsearch.database.entity.Airport;
import com.tech.challenge.flightsearch.database.repository.AirportRepository;
import com.tech.challenge.flightsearch.models.dto.AirportDTO;
import com.tech.challenge.flightsearch.models.mappers.AirportMapper;
import com.tech.challenge.flightsearch.models.mappers.AirportMapperImpl;
import com.tech.challenge.flightsearch.modules.airport.provider.AirportProvider;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import reactor.test.StepVerifier;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;

public class AirportServiceTest {

    private final AirportRepository airportRepository = Mockito.mock(AirportRepository.class, Mockito.RETURNS_MOCKS);
    private final AirportMapper airportMapper = Mockito.mock(AirportMapperImpl.class, Mockito.CALLS_REAL_METHODS);
    private final AirportProvider airportProvider = new AirportProvider(airportRepository);
    private final AirportService airportService = new AirportService(airportProvider, airportMapper);

    private static final UUID uuid = UUID.randomUUID();

    private static Airport getAirport() {
        return Airport.builder().id(uuid).name("TEST").code("1234").country("COUNTRY").city("CITY").active(true).build();
    }

    private static AirportDTO getAirportDTO() {
        return AirportDTO.builder().id(uuid.toString()).name("TEST").code("1234").country("COUNTRY").city("CITY").active(true).build();
    }

    @Test
    void findAll() {
        Mockito.when(airportRepository.findAll(Sort.by(Sort.Direction.ASC, "name"))).thenReturn(List.of(getAirport()));
        StepVerifier.create(airportService.findAll())
                .recordWith(ArrayList::new)
                .expectNextCount(1L)
                .consumeRecordedWith(it -> {
                    assertEquals(1, it.size());
                    assertTrue(it.stream().findFirst().isPresent());
                })
                .verifyComplete();
    }

    @Test
    void findById() {
        Mockito.when(airportRepository.findById(uuid)).thenReturn(Optional.of(getAirport()));
        StepVerifier.create(airportService.findById(uuid.toString()))
                .consumeNextWith(it ->assertEquals(uuid.toString(), it.getId()))
                .verifyComplete();
    }

    @Test
    void save() {
        final var airportCaptor = ArgumentCaptor.forClass(Airport.class);
        AirportDTO airportDTO = getAirportDTO();
        airportDTO.setId(null);
        Airport airport = getAirport();
        Mockito.when(airportRepository.save(airportCaptor.capture())).thenReturn(airport);
        StepVerifier.create(airportService.save(airportDTO))
                .consumeNextWith(it -> {
                    final var savedAirport = airportCaptor.getValue();
                    assertNotNull(it.getId());
                    assertEquals(savedAirport.getCode(), it.getCode());
                })
                .verifyComplete();
    }

    @Test
    void update() {
        final var airportCaptor = ArgumentCaptor.forClass(Airport.class);
        Airport airport = getAirport();
        AirportDTO airportDTO = getAirportDTO();

        Mockito.when(airportRepository.save(airportCaptor.capture())).thenReturn(airport);
        StepVerifier.create(airportService.update(airportDTO))
                .consumeNextWith(it -> {
                    final var savedAirport = airportCaptor.getValue();
                    assertEquals(savedAirport.getId().toString(), it.getId());
                    assertEquals(savedAirport.getCode(), it.getCode());
                })
                .verifyComplete();
    }

    @Test
    void delete() {
        final var uuidSlot = ArgumentCaptor.forClass(UUID.class);
        Mockito.when(airportRepository.existsById(uuidSlot.capture())).thenReturn(true);
        Mockito.doNothing().when(airportRepository).deleteById(uuidSlot.capture());
        StepVerifier.create(airportService.delete(uuid.toString())).verifyComplete();
        assertEquals(uuid, uuidSlot.getValue());
    }

    @Test
    void findFirstByCode() {
        Airport airport = getAirport();
        Mockito.when(airportRepository.findFirstByCodeIgnoreCase(airport.getCode())).thenReturn(Optional.of(airport));
        StepVerifier.create(airportService.findFirstByCode(airport.getCode()))
                .consumeNextWith(it -> {
                    assertEquals(airport.getId().toString(), it.getId());
                    assertEquals(airport.getCode(), it.getCode());
                })
                .verifyComplete();
    }

    @Test
    void search() {
        Mockito.when(airportRepository.findAll((Specification<Airport>) any(), (Pageable) any())).thenReturn(new PageImpl<>(List.of(getAirport(), getAirport())));
        PageRequest pageRequest = PageRequest.of(0, 10, Sort.by(Sort.Direction.valueOf("ASC"), "name"));
        StepVerifier.create(airportService.search("TES", true, pageRequest))
                .consumeNextWith(it -> {
                    assertEquals(2L, it.getTotalElements());
                    assertTrue(it.stream().findFirst().isPresent());
                })
                .verifyComplete();
    }

}
