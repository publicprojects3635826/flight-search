package com.tech.challenge.flightsearch.models.error;

import com.tech.challenge.flightsearch.models.enums.ServiceError;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class ServiceException extends RuntimeException {

    private final HttpStatus status;

    public ServiceException(ServiceError error) {
        super(error.getMessage());
        this.status = error.getStatus();
    }
}
