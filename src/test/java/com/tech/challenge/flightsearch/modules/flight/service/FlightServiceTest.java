package com.tech.challenge.flightsearch.modules.flight.service;

import com.tech.challenge.flightsearch.database.entity.Flight;
import com.tech.challenge.flightsearch.database.repository.FlightRepository;
import com.tech.challenge.flightsearch.models.dto.FlightDTO;
import com.tech.challenge.flightsearch.models.dto.FlightRequestDTO;
import com.tech.challenge.flightsearch.models.mappers.FlightMapper;
import com.tech.challenge.flightsearch.models.mappers.FlightMapperImpl;
import com.tech.challenge.flightsearch.modules.airport.provider.AirportProvider;
import com.tech.challenge.flightsearch.modules.flight.provider.FlightProvider;
import com.tech.challenge.flightsearch.modules.flightcompany.provider.FlightCompanyProvider;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import reactor.test.StepVerifier;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;

public class FlightServiceTest {

    private final FlightRepository flightRepository = Mockito.mock(FlightRepository.class, Mockito.RETURNS_MOCKS);
    private final FlightMapper flightMapper = Mockito.mock(FlightMapperImpl.class, Mockito.CALLS_REAL_METHODS);
    private final FlightCompanyProvider flightCompanyProvider = Mockito.mock(FlightCompanyProvider.class, Mockito.RETURNS_MOCKS);
    private final AirportProvider airportProvider =Mockito.mock(AirportProvider.class, Mockito.RETURNS_MOCKS);
    private final FlightProvider flightProvider = new FlightProvider(flightRepository);
    private final FlightService flightService = new FlightService(flightProvider, flightMapper, flightCompanyProvider, airportProvider);

    private static final UUID uuid = UUID.randomUUID();

    @Test
    void findAll() {
        Mockito.when(flightRepository.findAll(Sort.by(Sort.Direction.ASC, "price"))).thenReturn(List.of(getFlight()));
        StepVerifier.create(flightService.findAll())
                .recordWith(ArrayList::new)
                .expectNextCount(1L)
                .consumeRecordedWith(it -> {
                    assertEquals(1, it.size());
                    assertTrue(it.stream().findFirst().isPresent());
                })
                .verifyComplete();
    }

    @Test
    void findById() {
        Mockito.when(flightRepository.findById(uuid)).thenReturn(Optional.of(getFlight()));
        StepVerifier.create(flightService.findById(uuid.toString()))
                .consumeNextWith(it ->assertEquals(uuid.toString(), it.getId()))
                .verifyComplete();
    }

    @Test
    void save() {
        final var flightCaptor = ArgumentCaptor.forClass(Flight.class);
        Flight flight = getFlight();
        Mockito.when(flightRepository.save(flightCaptor.capture())).thenReturn(flight);
        Mockito.when(flightRepository.findFirstByCodeIgnoreCase(Mockito.any())).thenReturn(Optional.empty());
        Mockito.when(flightCompanyProvider.existsById(Mockito.any())).thenReturn(true);
        Mockito.when(airportProvider.existsById(Mockito.any())).thenReturn(true);
        Mockito.when(airportProvider.existsById(Mockito.any())).thenReturn(true);
        StepVerifier.create(flightService.save(getFlightRequest()))
                .consumeNextWith(it -> {
                    final var savedFlight = flightCaptor.getValue();
                    assertNotNull(it.getId());
                    assertEquals(savedFlight.getCode(), it.getCode());
                })
                .verifyComplete();
    }

    @Test
    void update() {
        final var flightCaptor = ArgumentCaptor.forClass(Flight.class);
        UUID uuid = UUID.randomUUID();
        Flight flight = getFlight();
        flight.setId(uuid);
        Mockito.when(flightRepository.save(flightCaptor.capture())).thenReturn(flight);
        Mockito.when(flightCompanyProvider.existsById(Mockito.any())).thenReturn(true);
        Mockito.when(airportProvider.existsById(Mockito.any())).thenReturn(true);
        Mockito.when(airportProvider.existsById(Mockito.any())).thenReturn(true);
        FlightRequestDTO flightRequest = getFlightRequest();
        flightRequest.setId(uuid.toString());
        StepVerifier.create(flightService.update(flightRequest))
                .consumeNextWith(it -> {
                    final var savedFlight = flightCaptor.getValue();
                    assertEquals(savedFlight.getId().toString(), it.getId());
                    assertEquals(savedFlight.getCode(), it.getCode());
                })
                .verifyComplete();
    }

    @Test
    void delete() {
        final var uuidSlot = ArgumentCaptor.forClass(UUID.class);
        Mockito.when(flightRepository.existsById(uuidSlot.capture())).thenReturn(true);
        Mockito.doNothing().when(flightRepository).deleteById(uuidSlot.capture());
        StepVerifier.create(flightService.delete(uuid.toString())).verifyComplete();
        assertEquals(uuid, uuidSlot.getValue());
    }

    @Test
    void findFirstByCode() {
        Flight flight = getFlight();
        Mockito.when(flightRepository.findFirstByCodeIgnoreCase(flight.getCode())).thenReturn(Optional.of(flight));
        StepVerifier.create(flightService.findFirstByCode(flight.getCode()))
                .consumeNextWith(it -> {
                    assertEquals(flight.getId().toString(), it.getId());
                    assertEquals(flight.getCode(), it.getCode());
                })
                .verifyComplete();
    }

    @Test
    void search() {
        Mockito.when(flightRepository.findAll((Specification<Flight>) any(), (Pageable) any())).thenReturn(new PageImpl<>(List.of(getFlight(), getFlight())));
        PageRequest pageRequest = PageRequest.of(0, 10, Sort.by(Sort.Direction.valueOf("ASC"), "name"));
        StepVerifier.create(flightService.search("TES", true, pageRequest))
                .consumeNextWith(it -> {
                    assertEquals(2L, it.getTotalElements());
                    assertTrue(it.stream().findFirst().isPresent());
                })
                .verifyComplete();
    }

    private static FlightRequestDTO getFlightRequest() {
        return FlightRequestDTO.builder().code("1234").price(BigDecimal.TEN).active(true).flightCompanyId(UUID.randomUUID().toString()).toAirportId(UUID.randomUUID().toString()).fromAirportId(UUID.randomUUID().toString()).build();
    }


    private static Flight getFlight() {
        return Flight.builder().id(uuid).code("1234").active(true).build();
    }

    private static FlightDTO getFlightDTO() {
        return FlightDTO.builder().id(uuid.toString()).code("1234").active(true).build();
    }

}
