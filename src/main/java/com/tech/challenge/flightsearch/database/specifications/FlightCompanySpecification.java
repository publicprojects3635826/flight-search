package com.tech.challenge.flightsearch.database.specifications;

import com.tech.challenge.flightsearch.database.entity.FlightCompany;
import io.micrometer.common.util.StringUtils;
import org.springframework.data.jpa.domain.Specification;

public class FlightCompanySpecification {

    public Specification<FlightCompany> search(String term) {
        final var search = StringUtils.isBlank(term) ? "" : term;
        return (root, query, criteriaBuilder) -> criteriaBuilder.or(
                criteriaBuilder.like(criteriaBuilder.lower(root.get("name")), "%" + search.toLowerCase() + "%"),
                criteriaBuilder.like(criteriaBuilder.lower(root.get("code")), "%" + search.toLowerCase() + "%")
        );
    }

    public Specification<FlightCompany> active(boolean active) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("active"), active);
    }

}
