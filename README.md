# Getting Started

### Reference Documentation
For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/3.3.1/maven-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/3.3.1/maven-plugin/reference/html/#build-image)
* [Spring Data JPA](https://docs.spring.io/spring-boot/docs/3.3.1/reference/htmlsingle/index.html#data.sql.jpa-and-spring-data)
* [Spring Boot DevTools](https://docs.spring.io/spring-boot/docs/3.3.1/reference/htmlsingle/index.html#using.devtools)
* [Liquibase Migration](https://docs.spring.io/spring-boot/docs/3.3.1/reference/htmlsingle/index.html#howto.data-initialization.migration-tool.liquibase)
* [Spring Security](https://docs.spring.io/spring-boot/docs/3.3.1/reference/htmlsingle/index.html#web.security)
* [Spring Web](https://docs.spring.io/spring-boot/docs/3.3.1/reference/htmlsingle/index.html#web)
* [Spring WebFlux](https://docs.spring.io/spring-framework/reference/web/webflux.html#web)
* [MapStruct](https://mapstruct.org/#web)
* [Docker](https://www.docker.com/#web)

### Guides
The following guides illustrate how to use some features concretely:

* [Accessing Data with JPA](https://spring.io/guides/gs/accessing-data-jpa/)
* [Securing a Web Application](https://spring.io/guides/gs/securing-web/)
* [Spring Boot and OAuth2](https://spring.io/guides/tutorials/spring-boot-oauth2/)
* [Authenticating a User with LDAP](https://spring.io/guides/gs/authenticating-ldap/)
* [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/)
* [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)
* [Building REST services with Spring](https://spring.io/guides/tutorials/rest/)

### Maven Parent overrides

Due to Maven's design, elements are inherited from the parent POM to the project POM.
While most of the inheritance is fine, it also inherits unwanted elements like `<license>` and `<developers>` from the parent.
To prevent this, the project POM contains empty overrides for these elements.
If you manually switch to a different parent and actually want the inheritance, you need to remove those overrides.

### Run Application on Docker
The application use liquibase and already has some pre-registration values on airport, flight company and flight tables, you can check on the below files.
- [V1_002__inser_airports](src/main/resources/db/migrations/V1_002__inser_airports.yml)
- [V1_005__inser_flight_company](src/main/resources/db/migrations/V1_005__inser_flight_company.yml)
- [V1_007__inser_flight](src/main/resources/db/migrations/V1_007__inser_flight.yml)


1. [Dockerfile](Dockerfile) has the configuration to build a package and generate a docker image to be deployed
2. Run [docker-compose](docker-compose.yml) file to have a PostgreSQL Database and API running locale
2. Access [Swagger](http://localhost:8080/swagger-ui/index.html#/)
3. This project have 3 in-memory users, each with distinct role. You can check on [WebSecurityConfiguration.java](src/main/java/com/tech/challenge/flightsearch/config/WebSecurityConfiguration.java)
4. The environment configs related to DB are in [local.env](local.env)

