package com.tech.challenge.flightsearch.database.entity;

import jakarta.persistence.*;
import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EntityListeners(AuditingEntityListener.class)
@Table(name = "flight")
public class Flight implements Serializable {
    @Id
    @Column(name = "flight_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    UUID id;
    @OneToOne(targetEntity = FlightCompany.class, fetch = FetchType.EAGER)
    @JoinColumn(name = "flight_company_id")
    FlightCompany flightCompany;
    @Column(name = "code", nullable = false)
    String code;
    @Column(name = "price")
    BigDecimal price;
    @OneToOne(targetEntity = Airport.class, fetch = FetchType.EAGER)
    @JoinColumn(name = "from_airport_id")
    Airport fromAirport;
    @OneToOne(targetEntity = Airport.class, fetch = FetchType.EAGER)
    @JoinColumn(name = "to_airport_id")
    Airport toAirport;
    @Column(name = "active", nullable = false)
    Boolean active;
    @CreatedDate
    @Column(name = "created_date")
    LocalDateTime createdDate;
    @LastModifiedDate
    @Column(name = "updated_date")
    LocalDateTime updatedDate;
}
