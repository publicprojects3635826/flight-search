package com.tech.challenge.flightsearch.modules.flight.provider;

import com.tech.challenge.flightsearch.database.entity.Flight;
import com.tech.challenge.flightsearch.database.repository.FlightRepository;
import com.tech.challenge.flightsearch.database.specifications.FlightSpecification;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class FlightProvider {

    private final FlightRepository flightRepository;

    public List<Flight> findAll() {
        return flightRepository.findAll(Sort.by(Sort.Direction.ASC, "price"));
    }

    public Optional<Flight> findById(UUID id) {
        return flightRepository.findById(id);
    }

    public Flight save(Flight airport) {
        return flightRepository.save(airport);
    }

    public void delete(UUID uuid) {
        flightRepository.deleteById(uuid);
    }

    public boolean existsById(UUID id) {
        return flightRepository.existsById(id);
    }

    public Optional<Flight> findFirstByCode(String code) {
        return flightRepository.findFirstByCodeIgnoreCase(code);
    }

    public Page<Flight> search(String term, Boolean active, Pageable pageable) {
        final var specification = new FlightSpecification();
        var searchFlightSpecification = specification.search(term);
        if (Objects.nonNull(active))
            searchFlightSpecification = searchFlightSpecification.and(specification.active(active));
        return flightRepository.findAll(searchFlightSpecification, pageable);
    }

    public Page<Flight> searchBestPrice(String from, String to, Pageable pageable) {
        final var specification = new FlightSpecification();
        var searchFlightSpecification = specification.searchFrom(from).and(specification.searchTo(to));
        return flightRepository.findAll(searchFlightSpecification, pageable);
    }

}
