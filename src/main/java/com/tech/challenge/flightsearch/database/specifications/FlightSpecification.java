package com.tech.challenge.flightsearch.database.specifications;

import com.tech.challenge.flightsearch.database.entity.Flight;
import io.micrometer.common.util.StringUtils;
import org.springframework.data.jpa.domain.Specification;

public class FlightSpecification {

    public Specification<Flight> search(String term) {
        final var search = StringUtils.isBlank(term) ? "" : term;
        return (root, query, criteriaBuilder) -> criteriaBuilder.or(
                criteriaBuilder.like(criteriaBuilder.lower(root.get("fromAirport").get("code")), "%" + search.toLowerCase() + "%"),
                criteriaBuilder.like(criteriaBuilder.lower(root.get("fromAirport").get("name")), "%" + search.toLowerCase() + "%"),
                criteriaBuilder.like(criteriaBuilder.lower(root.get("fromAirport").get("city")), "%" + search.toLowerCase() + "%"),
                criteriaBuilder.like(criteriaBuilder.lower(root.get("fromAirport").get("country")), "%" + search.toLowerCase() + "%"),
                criteriaBuilder.like(criteriaBuilder.lower(root.get("toAirport").get("code")), "%" + search.toLowerCase() + "%"),
                criteriaBuilder.like(criteriaBuilder.lower(root.get("toAirport").get("name")), "%" + search.toLowerCase() + "%"),
                criteriaBuilder.like(criteriaBuilder.lower(root.get("toAirport").get("city")), "%" + search.toLowerCase() + "%"),
                criteriaBuilder.like(criteriaBuilder.lower(root.get("toAirport").get("country")), "%" + search.toLowerCase() + "%"),
                criteriaBuilder.like(criteriaBuilder.lower(root.get("code")), "%" + search.toLowerCase() + "%")
        );
    }

    public Specification<Flight> active(boolean active) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("active"), active);
    }

    public Specification<Flight> searchFrom(String term) {
        final var search = StringUtils.isBlank(term) ? "" : term;
        return (root, query, criteriaBuilder) -> criteriaBuilder.or(
                criteriaBuilder.like(criteriaBuilder.lower(root.get("fromAirport").get("code")), "%" + search.toLowerCase() + "%"),
                criteriaBuilder.like(criteriaBuilder.lower(root.get("fromAirport").get("name")), "%" + search.toLowerCase() + "%"),
                criteriaBuilder.like(criteriaBuilder.lower(root.get("fromAirport").get("city")), "%" + search.toLowerCase() + "%"),
                criteriaBuilder.like(criteriaBuilder.lower(root.get("fromAirport").get("country")), "%" + search.toLowerCase() + "%")
        );
    }

    public Specification<Flight> searchTo(String term) {
        final var search = StringUtils.isBlank(term) ? "" : term;
        return (root, query, criteriaBuilder) -> criteriaBuilder.or(
                criteriaBuilder.like(criteriaBuilder.lower(root.get("toAirport").get("code")), "%" + search.toLowerCase() + "%"),
                criteriaBuilder.like(criteriaBuilder.lower(root.get("toAirport").get("name")), "%" + search.toLowerCase() + "%"),
                criteriaBuilder.like(criteriaBuilder.lower(root.get("toAirport").get("city")), "%" + search.toLowerCase() + "%"),
                criteriaBuilder.like(criteriaBuilder.lower(root.get("toAirport").get("country")), "%" + search.toLowerCase() + "%")
        );
    }

}
