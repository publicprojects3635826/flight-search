package com.tech.challenge.flightsearch.database.repository;

import com.tech.challenge.flightsearch.database.entity.Flight;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface FlightRepository extends JpaRepository<Flight, UUID>, JpaSpecificationExecutor<Flight> {

    Optional<Flight> findFirstByCodeIgnoreCase(String code);

}
