package com.tech.challenge.flightsearch.modules.flightcompany.provider;

import com.tech.challenge.flightsearch.database.entity.FlightCompany;
import com.tech.challenge.flightsearch.database.repository.FlightCompanyRepository;
import com.tech.challenge.flightsearch.database.specifications.FlightCompanySpecification;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class FlightCompanyProvider {

    private final FlightCompanyRepository flightCompanyRepository;

    public List<FlightCompany> findAll() {
        return flightCompanyRepository.findAll(Sort.by(Sort.Direction.ASC, "name"));
    }

    public Optional<FlightCompany> findById(UUID id) {
        return flightCompanyRepository.findById(id);
    }

    public FlightCompany save(FlightCompany airport) {
        return flightCompanyRepository.save(airport);
    }

    public void delete(UUID uuid) {
        flightCompanyRepository.deleteById(uuid);
    }

    public boolean existsById(UUID id) {
        return flightCompanyRepository.existsById(id);
    }

    public Optional<FlightCompany> findFirstByCode(String code) {
        return flightCompanyRepository.findFirstByCodeIgnoreCase(code);
    }

    public Page<FlightCompany> search(String term, Boolean active, Pageable pageable) {
        final var specification = new FlightCompanySpecification();
        var searchFlightCompanySpecification = specification.search(term);
        if (Objects.nonNull(active))
            searchFlightCompanySpecification = searchFlightCompanySpecification.and(specification.active(active));
        return flightCompanyRepository.findAll(searchFlightCompanySpecification, pageable);
    }

}
