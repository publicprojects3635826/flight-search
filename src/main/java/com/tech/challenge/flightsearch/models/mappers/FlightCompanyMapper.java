package com.tech.challenge.flightsearch.models.mappers;

import com.tech.challenge.flightsearch.database.entity.FlightCompany;
import com.tech.challenge.flightsearch.models.dto.FlightCompanyDTO;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING,
        unmappedTargetPolicy = ReportingPolicy.IGNORE
)
public interface FlightCompanyMapper extends BaseMapper<FlightCompany, FlightCompanyDTO> {
}
