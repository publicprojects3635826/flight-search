package com.tech.challenge.flightsearch.models.mappers;

import org.mapstruct.MappingTarget;

import java.util.List;

interface BaseMapper<E, D> {

    D toDto(E entity);

    E toEntity(D dto);

    List<D> toDto(List<E> entities);

    List<D> toDto(Iterable<E> entities);

    List<E> toEntity(List<D> dtos);

    void fromDto(D dto, @MappingTarget E entity);

}
