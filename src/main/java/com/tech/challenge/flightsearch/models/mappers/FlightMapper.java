package com.tech.challenge.flightsearch.models.mappers;

import com.tech.challenge.flightsearch.database.entity.Flight;
import com.tech.challenge.flightsearch.models.dto.FlightDTO;
import com.tech.challenge.flightsearch.models.dto.FlightRequestDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING,
        unmappedTargetPolicy = ReportingPolicy.IGNORE
)
public interface FlightMapper extends BaseMapper<Flight, FlightDTO> {

    @Mapping(target = "flightCompany.id", source = "flightCompanyId")
    @Mapping(target = "fromAirport.id", source = "fromAirportId")
    @Mapping(target = "toAirport.id", source = "toAirportId")
    Flight fromRequestDTO(FlightRequestDTO flightDTO);
}
