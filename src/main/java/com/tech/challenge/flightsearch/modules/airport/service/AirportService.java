package com.tech.challenge.flightsearch.modules.airport.service;

import com.tech.challenge.flightsearch.models.dto.AirportDTO;
import com.tech.challenge.flightsearch.models.enums.ServiceError;
import com.tech.challenge.flightsearch.models.error.ServiceException;
import com.tech.challenge.flightsearch.models.mappers.AirportMapper;
import com.tech.challenge.flightsearch.modules.airport.provider.AirportProvider;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Objects;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class AirportService {

    private final AirportProvider airportProvider;
    private final AirportMapper airportMapper;

    public Flux<AirportDTO> findAll() {
        return Flux.fromIterable(airportProvider.findAll())
                .map(airportMapper::toDto);
    }

    public Mono<AirportDTO> findById(String id) {
        return Mono.fromCallable(() -> UUID.fromString(id))
                .onErrorMap(error -> new ServiceException(ServiceError.BAD_REQUEST))
                .mapNotNull(uuid -> airportProvider.findById(uuid).orElse(null))
                .switchIfEmpty(Mono.error(new ServiceException(ServiceError.AIRPORT_NOT_FOUND)))
                .map(airportMapper::toDto);
    }

    public Mono<AirportDTO> save(AirportDTO airportDTO) {
        return Mono.fromCallable(() -> airportMapper.toEntity(airportDTO))
                .onErrorMap(error -> new ServiceException(ServiceError.BAD_REQUEST))
                .doOnNext(airport -> {
                    if (Objects.nonNull(airport.getId()))
                        throw new ServiceException(ServiceError.AIRPORT_ID_NOT_EMPTY);
                    if (airportProvider.findFirstByCode(airport.getCode()).isPresent())
                        throw new ServiceException(ServiceError.AIRPORT_EXIST);
                })
                .map(airportProvider::save)
                .map(airportMapper::toDto);

    }

    public Mono<AirportDTO> update(AirportDTO airportDTO) {
        return Mono.fromCallable(() -> airportMapper.toEntity(airportDTO))
                .onErrorMap(error -> new ServiceException(ServiceError.BAD_REQUEST))
                .doOnNext(airport -> {
                    if (Objects.isNull(airport.getId()))
                        throw new ServiceException(ServiceError.AIRPORT_ID_NOT_EMPTY);
                })
                .map(airportProvider::save)
                .map(airportMapper::toDto);

    }

    public Mono<Void> delete(String id) {
        return Mono.fromCallable(() -> UUID.fromString(id))
                .onErrorMap(error -> new ServiceException(ServiceError.BAD_REQUEST))
                .doOnNext(uuid -> {
                    if (!airportProvider.existsById(uuid))
                        throw new ServiceException(ServiceError.AIRPORT_NOT_FOUND);
                })
                .doOnNext(airportProvider::delete)
                .then();

    }

    public Mono<AirportDTO> findFirstByCode(String code) {
        return Mono.fromCallable(() -> airportProvider.findFirstByCode(code).orElse(null))
                .switchIfEmpty(Mono.error(new ServiceException(ServiceError.AIRPORT_NOT_FOUND)))
                .map(airportMapper::toDto);
    }

    public Mono<Page<AirportDTO>> search(String term, Boolean active, Pageable pageable) {
        return Mono.fromCallable(() -> airportProvider.search(term, active, pageable))
                .map(page -> page.map(airportMapper::toDto));
    }
}
