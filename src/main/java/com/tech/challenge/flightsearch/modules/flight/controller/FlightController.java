package com.tech.challenge.flightsearch.modules.flight.controller;

import com.tech.challenge.flightsearch.models.dto.FlightDTO;
import com.tech.challenge.flightsearch.models.dto.FlightRequestDTO;
import com.tech.challenge.flightsearch.models.enums.Permission;
import com.tech.challenge.flightsearch.modules.flight.service.FlightService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Validated
@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@RequestMapping("/flight")
@Tag(name = "Flight", description = "Flight controller")
public class FlightController {

    private final FlightService flightService;

    @Secured(value = {Permission.Constants.ADMIN, Permission.Constants.AIRPORT})
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @Operation(
            description = "Search all flights",
            responses = {
                    @ApiResponse(responseCode = "200", description = "return list of flights",
                            content = {@Content(array = @ArraySchema(schema = @Schema(implementation = FlightDTO.class)))}
                    ),
                    @ApiResponse(responseCode = "401", description = "unauthorized"),
                    @ApiResponse(responseCode = "403", description = "forbidding"),
            }
    )
    public Flux<FlightDTO> findAll() {
        return flightService.findAll();
    }



    @Secured(value = {Permission.Constants.USER})
    @GetMapping(value = "search-best-price", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @Operation(
            description = "Search flight best price",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Return flights based on locale from and to",
                            content = @Content(array = @ArraySchema(schema = @Schema(implementation = FlightDTO.class)))
                    ),
                    @ApiResponse(responseCode = "401", description = "unauthorized")
            }
    )
    public Mono<Page<FlightDTO>> getBestPrice(
            @RequestParam(value = "from") String from,
            @RequestParam(name = "to") String to,
            @RequestParam(value = "page", defaultValue = "0") Integer page,
            @RequestParam(value = "size", defaultValue = "10") Integer size,
            @RequestParam(value = "sort", defaultValue = "price") String sort,
            @RequestParam(value = "direction", defaultValue = "ASC") String direction
    ) {
        return flightService.searchBestPrice(from, to, PageRequest.of(page, size, Sort.by(Sort.Direction.valueOf(direction), sort)));
    }

    @Secured(value = {Permission.Constants.USER})
    @GetMapping(value = "search", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @Operation(
            description = "Search flight by term",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Return flights paginated",
                            content = @Content(array = @ArraySchema(schema = @Schema(implementation = FlightDTO.class)))
                    ),
                    @ApiResponse(responseCode = "401", description = "unauthorized")
            }
    )
    public Mono<Page<FlightDTO>> search(
            @RequestParam(value = "term", required = false) String term,
            @RequestParam(name = "active", required = false) Boolean active,
            @RequestParam(value = "page", defaultValue = "0") Integer page,
            @RequestParam(value = "size", defaultValue = "10") Integer size,
            @RequestParam(value = "sort", defaultValue = "price") String sort,
            @RequestParam(value = "direction", defaultValue = "ASC") String direction
    ) {
        return flightService.search(term, active, PageRequest.of(page, size, Sort.by(Sort.Direction.valueOf(direction), sort)));
    }

    @Secured(value = {Permission.Constants.ADMIN, Permission.Constants.AIRPORT})
    @GetMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @Operation(
            description = "Search flight by UUID",
            responses = {
                    @ApiResponse(responseCode = "200", description = "return flight",
                            content = {@Content(array = @ArraySchema(schema = @Schema(implementation = FlightDTO.class)))}
                    ),
                    @ApiResponse(responseCode = "401", description = "unauthorized"),
                    @ApiResponse(responseCode = "403", description = "forbidding"),
                    @ApiResponse(responseCode = "404", description = "flight not found"),
            }
    )
    public Mono<FlightDTO> findById(@PathVariable String id) {
        return flightService.findById(id);
    }

    @Secured(value = {Permission.Constants.USER})
    @GetMapping(value = "by-code/{code}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @Operation(
            description = "Search flight by code",
            responses = {
                    @ApiResponse(responseCode = "200", description = "return flight",
                            content = {@Content(array = @ArraySchema(schema = @Schema(implementation = FlightDTO.class)))}
                    ),
                    @ApiResponse(responseCode = "401", description = "unauthorized"),
                    @ApiResponse(responseCode = "404", description = "flight not found"),
            }
    )
    public Mono<FlightDTO> findFirstByCode(@PathVariable() String code) {
        return flightService.findFirstByCode(code);
    }

    @Secured(value = {Permission.Constants.ADMIN})
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(
            description = "Create new flight",
            responses = {
                    @ApiResponse(responseCode = "201", description = "return flight",
                            content = @Content(schema = @Schema(implementation = FlightDTO.class))
                    ),
                    @ApiResponse(responseCode = "400", description = "flight id not empty"),
                    @ApiResponse(responseCode = "401", description = "unauthorized"),
                    @ApiResponse(responseCode = "403", description = "forbidding")
            }
    )
    public Mono<FlightDTO> save(@Valid @RequestBody FlightRequestDTO flightDTO) {
        return flightService.save(flightDTO);
    }

    @Secured(value = {Permission.Constants.ADMIN})
    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(
            description = "Update existing flight",
            responses = {
                    @ApiResponse(responseCode = "201", description = "return flight",
                            content = @Content(schema = @Schema(implementation = FlightDTO.class))
                    ),
                    @ApiResponse(responseCode = "400", description = "flight id is empty"),
                    @ApiResponse(responseCode = "401", description = "unauthorized"),
                    @ApiResponse(responseCode = "403", description = "forbidding")
            }
    )
    public Mono<FlightDTO> update(@Valid @RequestBody FlightRequestDTO flightDTO) {
        return flightService.update(flightDTO);
    }

    @Secured(value = {Permission.Constants.ADMIN})
    @DeleteMapping(value = "{id}")
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(
            description = "Delete existing flight by ID",
            responses = {
                    @ApiResponse(responseCode = "201", description = "return flight",
                            content = @Content(schema = @Schema(implementation = FlightDTO.class))
                    ),
                    @ApiResponse(responseCode = "400", description = "flight id is empty"),
                    @ApiResponse(responseCode = "403", description = "forbidding")
            }
    )
    public Mono<Void> delete(@Valid @PathVariable String id) {
        return flightService.delete(id);
    }

}
