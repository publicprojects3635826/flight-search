package com.tech.challenge.flightsearch.models.enums;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum Permission {

    USER(Constants.USER),
    ADMIN(Constants.ADMIN),
    AIRPORT(Constants.AIRPORT),
    CLIENT(Constants.CLIENT);;

    private final String value;

    public static class Constants {
        private Constants() {
        }
        public static final String USER = "ROLE_USER";
        public static final String ADMIN = "ROLE_ADMIN";
        public static final String AIRPORT = "ROLE_AIRPORT";
        public static final String CLIENT = "ROLE_CLIENT";
    }

}
