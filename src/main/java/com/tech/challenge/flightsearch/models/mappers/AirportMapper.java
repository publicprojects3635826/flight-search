package com.tech.challenge.flightsearch.models.mappers;

import com.tech.challenge.flightsearch.database.entity.Airport;
import com.tech.challenge.flightsearch.models.dto.AirportDTO;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;
import org.mapstruct.ReportingPolicy;

import java.util.Optional;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING,
        unmappedTargetPolicy = ReportingPolicy.IGNORE
)
public interface AirportMapper extends BaseMapper<Airport, AirportDTO> {
}
