package com.tech.challenge.flightsearch.database.repository;

import com.tech.challenge.flightsearch.database.entity.FlightCompany;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface FlightCompanyRepository extends JpaRepository<FlightCompany, UUID>, JpaSpecificationExecutor<FlightCompany> {

    Optional<FlightCompany> findFirstByCodeIgnoreCase(String code);

}
