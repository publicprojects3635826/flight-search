package com.tech.challenge.flightsearch.models.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FlightDTO {
    @JsonProperty("id")
    String id;
    @JsonProperty("flight_company_id")
    FlightCompanyDTO flightCompany;
    @JsonProperty("code")
    String code;
    @JsonProperty("price")
    BigDecimal price;
    @JsonProperty("from_airport_id")
    AirportDTO fromAirport;
    @JsonProperty("to_airport_id")
    AirportDTO toAirport;
    @JsonProperty("active")
    Boolean active;
}
