FROM maven:3.8.4-openjdk-17-slim AS build

WORKDIR /app

COPY pom.xml .
COPY src ./src

RUN mvn clean package -DskipTests

FROM openjdk:17.0.2-slim-buster
WORKDIR /app

COPY --from=build /app/target/flight-search.jar /app/app.jar

RUN adduser --system spring
RUN addgroup spring
USER spring:spring

EXPOSE 8080

CMD ["sh", "-c", "java -jar app.jar"]
