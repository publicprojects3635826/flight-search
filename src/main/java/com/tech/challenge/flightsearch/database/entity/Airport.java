package com.tech.challenge.flightsearch.database.entity;

import jakarta.persistence.*;
import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EntityListeners(AuditingEntityListener.class)
@Table(name = "airport")
public class Airport implements Serializable {
    @Id
    @Column(name = "airport_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    UUID id;
    @Column(name = "name", nullable = false)
    String name;
    @Column(name = "code", nullable = false)
    String code;
    @Column(name = "country", nullable = false)
    String country;
    @Column(name = "city", nullable = false)
    String city;
    @Column(name = "active", nullable = false)
    Boolean active;
    @CreatedDate
    @Column(name = "created_date")
    LocalDateTime createdDate;
    @LastModifiedDate
    @Column(name = "updated_date")
    LocalDateTime updatedDate;
}
