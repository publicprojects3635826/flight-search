package com.tech.challenge.flightsearch.modules.airport.provider;

import com.tech.challenge.flightsearch.database.entity.Airport;
import com.tech.challenge.flightsearch.database.repository.AirportRepository;
import com.tech.challenge.flightsearch.database.specifications.AirportSpecification;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class AirportProvider {

    private final AirportRepository airportRepository;

    public List<Airport> findAll() {
        return airportRepository.findAll(Sort.by(Sort.Direction.ASC, "name"));
    }

    public Optional<Airport> findById(UUID id) {
        return airportRepository.findById(id);
    }

    public Airport save(Airport airport) {
        return airportRepository.save(airport);
    }

    public void delete(UUID uuid) {
        airportRepository.deleteById(uuid);
    }

    public boolean existsById(UUID id) {
        return airportRepository.existsById(id);
    }

    public Optional<Airport> findFirstByCode(String code) {
        return airportRepository.findFirstByCodeIgnoreCase(code);
    }

    public Page<Airport> search(String term, Boolean active, Pageable pageable) {
        final var specification = new AirportSpecification();
        var searchAirportSpecification = specification.search(term);
        if (Objects.nonNull(active))
            searchAirportSpecification = searchAirportSpecification.and(specification.active(active));
        return airportRepository.findAll(searchAirportSpecification, pageable);
    }

}
